# Nifi-App-Monitoring

  NIFI template to monitor the application status using the generated NIFI bulletin. This template uses S2S protocol for reporting tasks,   i.e. by using the input
  port on the canvas the reporting tasks are collected in the NIFI works flow. The bulletins are then seperated according to the process,   team, usecase, components
  and by many more categories. By this way it is possible to get fine grain control over the reporting metrics and the particular metrics   can by reported to 
  the respective development teams according to the Log level.